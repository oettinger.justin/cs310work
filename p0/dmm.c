#include <stdio.h>    // needed for size_t
#include <sys/mman.h> // needed for mmap
#include <assert.h>   // needed for asserts
#include "dmm.h"

/* You can improve the below metadata structure using the concepts from Bryant
 * and OHallaron book (chapter 9).
 */

typedef struct metadata
{
  /* size_t is the return type of the sizeof operator. Since the size of an
   * object depends on the architecture and its implementation, size_t is used
   * to represent the maximum size of any object in the particular
   * implementation. size contains the size of the data object or the number of
   * free bytes
   */
  size_t size;
  struct metadata *next;
  struct metadata *prev;
} metadata_t;

/* freelist maintains all the blocks which are not in use; freelist is kept
 * sorted to improve coalescing efficiency 
 */

static metadata_t *freelist = NULL;
static metadata_t *prologue = NULL; //check to see if this is correct or if these need to be declared below
static metadata_t *epilogue = NULL;

void *dmalloc(size_t numbytes)
{
  /* initialize through mmap call first time */
  if (freelist == NULL)
  {
    if (!dmalloc_init())
      return NULL;
  }
  //print_freelist();
  //printf("printed freelist\n"); //why isnt this workin
  assert(numbytes > 0);

  /* your code here */
  metadata_t *searcher = prologue;
  int bestfit = MAX_HEAP_SIZE;
  int headerSize = sizeof(metadata_t);
  int alignedVal = ALIGN(numbytes + headerSize);
  metadata_t *mallocLoc = NULL;
  while (1)
  {
    if (searcher->size >= alignedVal)
    { //might just be METADATA_T_ALIGNED
      if (searcher->size < bestfit)
      {
        mallocLoc = searcher;
        bestfit = searcher->size;
      }
    }
    if (searcher->next == epilogue)
    {
      break;
    }
    searcher = searcher->next;
  }
  if (mallocLoc == NULL)
  {
    return NULL;
  }
  else
  {
    size_t freeSpace = mallocLoc->size - alignedVal;
    //printf("Freespace = %zu\n", freeSpace);
    //printf("prologue = %p\n", prologue);
    metadata_t *newFree = (metadata_t *)((void *)mallocLoc + alignedVal);
    newFree->prev = mallocLoc->prev;
    newFree->next = mallocLoc->next;
    newFree->size = freeSpace;
    //printf("newFree location: %p\n", (void *)newFree);
    //printf("mallocLoc location: %p\n", (void *)mallocLoc);

    mallocLoc->prev->next = newFree;
    mallocLoc->next->prev = newFree;

    mallocLoc->prev = NULL;
    mallocLoc->next = NULL;
    //printf("Metadata size: %zu\n", METADATA_T_ALIGNED);
    //printf("MallocLoc size: %zu\n", mallocLoc->size);
    mallocLoc->size = alignedVal - METADATA_T_ALIGNED;
    //print_freelist();
    return ((void *)mallocLoc + METADATA_T_ALIGNED);
  }
}

void dfree(void *ptr)
{
  /* your code here */
  //int* dataLoc = (int*) ((long long) ptr - sizeof(metadata_t));
  //int dataSize = *dataLoc;
  //printf("started dfree\n");
  metadata_t *metaPtr = (metadata_t *)(ptr - METADATA_T_ALIGNED);
  //size_t dataSize = metaPtr->size;
  //printf("%zu\n", dataSize);

  metadata_t *itr = prologue;
  while ((((void *)itr) < metaPtr)){
    itr = itr->next;
  }
  itr = itr->prev;
  metaPtr->next = itr->next;
  metaPtr->prev = itr;
  itr->next = metaPtr;
  metaPtr->next->prev = metaPtr;
  //print_freelist();
  
  //COALESCE
  metadata_t* searcher = prologue;
  while(searcher != epilogue) {
    if(((metadata_t*) ((void*) metaPtr + metaPtr->size + METADATA_T_ALIGNED)) == searcher) {
        searcher->prev->next = searcher->next;
        metaPtr->size = metaPtr->size + searcher->size + METADATA_T_ALIGNED;
        searcher->next->prev = metaPtr;
        searcher->prev = NULL;
        searcher->next = NULL;
        searcher = NULL;
        break;
    }
    searcher = searcher->next;
  }
  searcher = prologue->next;
  while(searcher != epilogue) {
    if(((metadata_t*) ((void*) searcher + searcher->size + METADATA_T_ALIGNED)) == metaPtr) {
      searcher->next = metaPtr->next;
      searcher->size = metaPtr->size + searcher->size + METADATA_T_ALIGNED;
      metaPtr->next->prev = searcher;
      metaPtr->prev = NULL;
      metaPtr->next = NULL;
      metaPtr = NULL;
      break;
    }
    searcher = searcher->next;
  }
  metadata_t *itr2 = prologue;
  while ((((void *)itr2) < epilogue)){
    itr2 = itr2->next;
  }
  //print_freelist();

  //OLD COALESCE
  /*
  metadata_t* newFree = freelist;
  printf("created newFree");
  newFree->size = dataSize + sizeof(metadata_t);
  newFree->next = prologue->next;
  newFree->prev = prologue;
  prologue->next = newFree;
  printf("created newFree");
  metadata_t* searcher = prologue;
  while(1) {
    metadata_t* holdNext = searcher->next;
    if(((long long)searcher + searcher->size) == ((long long)newFree - sizeof(metadata_t))) {
      searcher->prev->next = searcher->next;
      searcher->prev = prologue;
      searcher->next = prologue->next;
      prologue->next->prev = searcher;
      prologue->next = searcher;
      searcher->size = searcher->size + newFree->size + sizeof(metadata_t);
      newFree = searcher;
      searcher = NULL;
    }
    else if(((long long)newFree + newFree->size) == ((long long)searcher - sizeof(metadata_t))) {
      searcher->prev->next = searcher->next;
      newFree->size = newFree->size + searcher->size + sizeof(metadata_t);
      searcher = NULL;
    }
    if(holdNext->next == NULL) {
      break;
    }
    else {
      searcher = holdNext;
    }
  }
  */
}

bool dmalloc_init()
{

  /* Two choices: 
   * 1. Append prologue and epilogue blocks to the start and the
   * end of the freelist 
   *
   * 2. Initialize freelist pointers to NULL
   *
   * Note: We provide the code for 2. Using 1 will help you to tackle the 
   * corner cases succinctly.
   */

  size_t max_bytes = ALIGN(MAX_HEAP_SIZE);
  /* returns heap_region, which is initialized to freelist */
  freelist = (metadata_t *)mmap(NULL, max_bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  /* Q: Why casting is used? i.e., why (void*)-1? */
  /*if (freelist == (void *)-1)
    return false;
  freelist->next = NULL;
  freelist->prev = NULL;
  freelist->size = max_bytes-METADATA_T_ALIGNED;
  return true;*/
  //orovided code for solution 2
  if (freelist == (void *)-1)
  {
    return false;
  }
  //metadata_t* prologue = NULL;
  //metadata_t* epilogue = NULL;
  prologue = (metadata_t *)freelist;
  //epilogue = (metadata_t*) freelist;
  freelist = (metadata_t *)((void *)freelist + METADATA_T_ALIGNED);
  freelist->size = max_bytes - 3 * (METADATA_T_ALIGNED);
  epilogue = (metadata_t *)((void *)freelist + freelist->size + METADATA_T_ALIGNED);
  prologue->next = freelist;
  prologue->prev = NULL;
  prologue->size = 0;
  epilogue->next = NULL;
  epilogue->prev = freelist;
  epilogue->size = 0;
  freelist->next = epilogue;
  freelist->prev = prologue;
  print_freelist();
  return true;
}

/* for debugging; can be turned off through -NDEBUG flag*/
void print_freelist()
{
  metadata_t *freelist_head = freelist;
  /*
  while (freelist_head != NULL)
  {
    printf("\tFreelist Size:%zd, Head:%p, Prev:%p, Next:%p\t",
           freelist_head->size,
           freelist_head,
           freelist_head->prev,
           freelist_head->next);
    freelist_head = freelist_head->next;
  }
  printf("\n");*/
  freelist_head = prologue;
  while (freelist_head != NULL)
  {
    printf("\tFreelist Size:%zd, Head:%p, Prev:%p, Next:%p\t",
           freelist_head->size,
           freelist_head,
           freelist_head->prev,
           freelist_head->next);
    freelist_head = freelist_head->next;
  }
  printf("\n");
}
