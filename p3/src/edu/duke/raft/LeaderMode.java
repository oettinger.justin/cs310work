package edu.duke.raft;
import java.util.concurrent.ThreadLocalRandom;
import java.util.*;


public class LeaderMode extends RaftMode {
  private Timer heartBeatTimer;
  private int timerID=22;
  
  //private int[] nextIndexes= new int[mConfig.getNumServers()+1];
  //ganna try to send it all at once


  public void go () {
    synchronized (mLock) {
      int term = mConfig.getCurrentTerm();
      System.out.println ("S" + 
			  mID + 
			  "." + 
			  term + 
			  ": switched to leader mode.");
      
      //updating term
      RaftResponses.setTerm(term);
      //clearing old responses
      RaftResponses.clearAppendResponses(term);


      Entry[] sending = logReplication();
     
      int numServers = mConfig.getNumServers();
      int lastLogTerm=mLog.getLastTerm();
  
      for (int voterID = 1; voterID <=numServers; voterID++) {
        if(voterID==mID){
          continue;
        }
        else{
          remoteAppendEntries(voterID,
          term,
          mID,
          -1,
          lastLogTerm, sending, mCommitIndex);
        }
      }
      heartBeatTimer=scheduleTimer(HEARTBEAT_INTERVAL, timerID);
    }
  }

  public Entry[] logReplication(){ // get my entries 
    Entry[] sending = new Entry[mLog.getLastIndex()+1];
    for(int i = 0; i <= mLog.getLastIndex(); i++){
      sending[i] = mLog.getEntry(i);
    }
    return sending;
  }
  
  // @param candidate’s term
  // @param candidate requesting vote
  // @param index of candidate’s last log entry
  // @param term of candidate’s last log entry
  // @return 0, if server votes for candidate; otherwise, server's
  // current term
  public int requestVote (int candidateTerm,
                          int candidateID,
                          int lastLogIndex,
                          int lastLogTerm) {
    synchronized (mLock) {
      int selfTerm=mConfig.getCurrentTerm();
      int whoVote = mConfig.getVotedFor();
      int myLastTerm = mLog.getLastTerm();
      int myLastIndex = mLog.getLastIndex();
      //if they are the better candidate return 0, change my term to theirs and go back to follower mode
      //if i am the better candidiate return my term --> change my term to theirs
      //didnt do the saftey check maybe should add because justin u said that leader sholdnt have voted anyway
      if(candidateTerm> selfTerm){
        
        //may need to step down regardless of logcheck, because term would then increment up to 
        //the recently seen max term, the other rando would lose the election because theyre not up
        //to date, and then this leader would win with the highest term and an up to date log

        if(logCheck(lastLogIndex, lastLogTerm)){
          heartBeatTimer.cancel();
          mConfig.setCurrentTerm(candidateTerm, candidateID);
          RaftServerImpl.setMode(new FollowerMode());
          return 0;
        }
        else{
           mConfig.setCurrentTerm(candidateTerm, 0);
          return selfTerm;
        }
      }
      else{
        return selfTerm;
      }     
    }
  }
  

  // @param leader’s term
  // @param current leader
  // @param index of log entry before entries to append
  // @param term of log entry before entries to append
  // @param entries to append (in order of 0 to append.length-1)
  // @param index of highest committed entry
  // @return 0, if server appended entries; otherwise, server's
  // current term
  public int appendEntries (int leaderTerm,
			    int leaderID,
			    int prevLogIndex,
			    int prevLogTerm,
			    Entry[] entries,
			    int leaderCommit) {
    synchronized (mLock) {
      int term = mConfig.getCurrentTerm ();
      if (leaderTerm > term) {
        mConfig.setCurrentTerm(leaderTerm, 0);
        heartBeatTimer.cancel();
        RaftServerImpl.setMode(new FollowerMode());
        return term;
      }
      else{
        //i think there's more stuff we may need to do here to guarantee safety but we'll see
        return term;
      }
    }
  }

  // @param id of the timer that timed out
  public void handleTimeout (int timerID) {
    synchronized (mLock) {
      if(timerID==22){
        heartBeatTimer.cancel();
              //send heartbeat 
        int selfID = mID;
        int numServers = mConfig.getNumServers();
        int term = mConfig.getCurrentTerm();
        int lastLogIndex = mLog.getLastIndex();
        int lastLogTerm = mLog.getLastTerm();
        RaftResponses.setTerm(term);
        RaftResponses.clearAppendResponses(term);

        Entry[] sending = logReplication();
  
        for (int voterID = 1; voterID <=numServers; voterID++) {
          if(voterID==mID){
           continue;
         }
         else{
          remoteAppendEntries(voterID,
          term,
          mID,
          -1,
          lastLogTerm, sending, mCommitIndex);
          }
        }
        heartBeatTimer=scheduleTimer(HEARTBEAT_INTERVAL, timerID);

      }
    }
  }

  public boolean logCheck(int lastLogIndex, int lastLogTerm){
    int myLastTerm = mLog.getLastTerm();
    int myLastIndex = mLog.getLastIndex();
    if(lastLogTerm>myLastTerm){
      //its upto date
      return true;
    }
    else if(lastLogIndex>=myLastIndex && lastLogTerm==myLastTerm){
      return true;
    }
    else{
      return false; //not up to date 
    }

  }
  
}


     // if(max>=term){ // i am not most updated 
     //   mConfig.setCurrentTerm(max, mID);
    //    RaftResponses.setTerm(max);
     //   heartBeatTimer.cancel();
    //    RaftServerImpl.setMode(new FollowerMode());
     // }