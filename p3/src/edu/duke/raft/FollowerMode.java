package edu.duke.raft;
import java.util.concurrent.ThreadLocalRandom;
import java.util.*;



//where do i increment term 
//where do i check for heartbeat messages 
//folower can only go to canidate mode 
//what do i need to do in the constructor 

//all servers 
//If commitIndex > lastApplied: increment lastApplied, apply
//log[lastApplied] to state machine (§5.3)
//******
//If RPC request or response contains term T > currentTerm:
//set currentTerm = T, convert to follower (§5.1) TO REST THINGS call GO again. 
//Respond to RPCs from candidates and leaders. 
//If election timeout elapses without receiving AppendEntries. RPC from current leader or granting vote to candidate:
//convert to candidate


public class FollowerMode extends RaftMode {
  private int timerTime = ThreadLocalRandom.current().nextInt(100, 150 + 1);
  //private int timerTime=200;
  int timerID=1;
  private Timer myTimer;
  
  public void go () { //init constructor 
    //start the timer 
    synchronized (mLock) {
      int term = mConfig.getCurrentTerm();
      System.out.println ("S" +
                          mID +
                          "." +
                          term +
                          ": switched to follower mode.");
       myTimer = scheduleTimer(timerTime,timerID);
    }
  }

  // @param candidate’s term
  // @param candidate requesting vote
  // @param index of candidate’s last log entry
  // @param term of candidate’s last log entry
  // @return 0, if server votes for candidate; otherwise, server's
  public int requestVote (int candidateTerm,
                          int candidateID,
                          int lastLogIndex,
                          int lastLogTerm) {
    //implement later check log
    //check if last log term is greater than yours 

    synchronized (mLock) {

      //DO I NEED TO DO TIMER STUFF HERE???

      int term = mConfig.getCurrentTerm();
      int whoVote = mConfig.getVotedFor();
      int myLastTerm = mLog.getLastTerm();
      int myLastIndex = mLog.getLastIndex();
      //where do i cancel the timer?

      if(candidateTerm>term){
        /*myTimer.cancel();
        myTimer = scheduleTimer(timerTime,timerID);*/

        //only cancel if their log is up to date as well
        if(whoVote==0 && logCheck(lastLogIndex, lastLogTerm)){
          myTimer.cancel();
          myTimer = scheduleTimer(timerTime,timerID);
          mConfig.setCurrentTerm(candidateTerm, candidateID);
          return 0;
        }
        else if(whoVote==candidateID && logCheck(lastLogIndex, lastLogTerm)){
          myTimer.cancel();
          myTimer = scheduleTimer(timerTime,timerID);
          mConfig.setCurrentTerm(candidateTerm, candidateID);
          return 0;
        }
        else{
          mConfig.setCurrentTerm(candidateTerm,0);
          return term;
        }
      }
      else{
        return term;
      }
      //have we voted 
      //myTimer=scheduleTimer(timerTime,timerID);

    }

 }

  // @param leader’s term
  // @param current leader
  // @param index of log entry before entries to append
  // @param term of log entry before entries to append
  // @param entries to append (in order of 0 to append.length-1)
  // @param index of highest committed entry
  // @return 0, if server appended entries; otherwise, server's
  // current term

 //must determine if requester is the current leader 
  public int appendEntries (int leaderTerm,
                            int leaderID,
                            int prevLogIndex,
                            int prevLogTerm,
                            Entry[] entries,
                            int leaderCommit) {
    synchronized(mLock) {
      //System.out.println(mID + ", " + leaderTerm + ", " + leaderID);
      //myTimer.cancel();
      int term = mConfig.getCurrentTerm();

      if(term>leaderTerm){ //my term is higher than theres 
        //myTimer=scheduleTimer(timerTime,timerID);
        return term;
      }
      else{  
        //they are a good leader 
        //check log 
        myTimer.cancel();
          mConfig.setCurrentTerm(leaderTerm,0);
          //RaftResponses.setTerm(leaderTerm);
          //Entry tempEntry = mLog.getEntry(prevLogIndex); //my entry at that index
          //check if its null and check if its most recentl try .term 

            //execute if -1 index is 
            // FIX LOG 
            // insert here 
          mLog.insert(entries, prevLogIndex, prevLogTerm);
          myTimer=scheduleTimer(timerTime,timerID);
          return 0;
      }
    }
  }

  // @param id of the timer that timed out
  public void handleTimeout (int timerID) {
    //if timer goes off, hold an election 
    synchronized (mLock) { 
      if(timerID==1){
        myTimer.cancel();
        RaftServerImpl.setMode(new CandidateMode());
      }
    }
  }

  public boolean logCheck(int lastLogIndex, int lastLogTerm){
    int myLastTerm = mLog.getLastTerm();
    int myLastIndex = mLog.getLastIndex();
    if(lastLogTerm>myLastTerm){
      //its upto date
      return true;
    }
    else if(lastLogIndex>=myLastIndex && lastLogTerm==myLastTerm){
      return true;
    }
    else{
      return false; //not up to date 
    }

  }


}
