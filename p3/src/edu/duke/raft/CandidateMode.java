package edu.duke.raft;
import java.util.concurrent.ThreadLocalRandom;
import java.util.*;
import java.util.Timer;
//one timer for the election // election timer
//one timer for the


public class CandidateMode extends RaftMode {
  //add a boolean for whether or not we've cancelled a timer
    private Timer bigTimer;
    private Timer smallTimer;
    private long electionLength;

  public void go () {
    synchronized (mLock) {
      int selfTerm=mConfig.getCurrentTerm();
      //int term = 0;
      //start new election by incrementing term and starting timer

      startElection();


      //this is the timer that will run the whole election
      //myCofig.setCurrentTerm(the new term, serverWhoSelfVotedFor)

      //commence voting
      }
    }


  // @param candidate’s term
  // @param candidate requesting vote
  // @param index of candidate’s last log entry
  // @param term of candidate’s last log entry
  // @return 0, if server votes for candidate; otherwise, server's
  // current term

//ask about how to start an election
  public void startElection() {

    int selfTerm=mConfig.getCurrentTerm();
    selfTerm++;
    mConfig.setCurrentTerm(selfTerm,0);
    RaftResponses.setTerm(selfTerm);

    //have to clear old votes
    RaftResponses.clearVotes(selfTerm);
    //start new election by incrementing term and starting timer
    System.out.println ("S" +
                        mID +
                        "." +
                        selfTerm +
                        ": switched to candidate mode.");

    //create a timer to check if we've garnered a majority of votes

    //what variables do we need in the election loop?
    //get inputs for remoteRequestVote
    //not sure about these ones (mostly serverID) vvv
    int selfID = mID;
    int lastLogIndex = mLog.getLastIndex();
    int lastLogTerm = mLog.getLastTerm();
    // ^^^^^
    int numServers = mConfig.getNumServers();

    //this loop sends out the vote requests to all other servers
    for (int voterID = 1; voterID <= numServers; voterID++) {
      if(voterID==mID){
        continue;
      }
      else{
        remoteRequestVote(voterID,
        selfTerm,
        selfID,
        lastLogIndex,
        lastLogTerm);
      }
    }
    electionLength = ThreadLocalRandom.current().nextLong(150, 300);
    int mainTimer = 2;
    bigTimer = scheduleTimer(electionLength, mainTimer);
    
    //readding our second timer back in because taking it out broke everything somehow
    int countTimer = 3;
    long electionTimerInterval = 25;
    smallTimer = scheduleTimer(electionTimerInterval, countTimer);
  }


  public int requestVote (int candidateTerm,
                          int candidateID,
                          int lastLogIndex,
                          int lastLogTerm) {
    synchronized (mLock) {
      int selfTerm=mConfig.getCurrentTerm();
      int whoVote = mConfig.getVotedFor();
      int myLastTerm = mLog.getLastTerm();
      int myLastIndex = mLog.getLastIndex();
      //if they are the better candidate return 0, change my term to theirs and go back to follower mode
      //if i am the better candidiate return my term --> change my term to theirs

      //i think we need to have more cases here
      if(candidateID==mID){ //vote forself idk if this is necessary might delete 
        mConfig.setCurrentTerm(selfTerm, mID);
        return 0;
      }
      if(candidateTerm>selfTerm){
        //check if uptolate
        //myLastTerm > lastLogTerm) || ((myLastTerm == lastLogTerm) && (myLastIndex > lastLogIndex same check
        //but now in a function
        if(logCheck(lastLogIndex, lastLogTerm)){
          bigTimer.cancel();
          smallTimer.cancel();
          mConfig.setCurrentTerm(candidateTerm, candidateID);
          RaftServerImpl.setMode(new FollowerMode());
          return 0;
        }
        else{
          mConfig.setCurrentTerm(candidateTerm, 0);
          return selfTerm;
        }

      }
      else{
        return selfTerm;
      }
    }
  }


  // @param leader’s term
  // @param current leader
  // @param index of log entry before entries to append
  // @param term of log entry before entries to append
  // @param entries to append (in order of 0 to append.length-1)
  // @param index of highest committed entry
  // @return 0, if server appended entries; otherwise, server's
  // current term
  public int appendEntries (int leaderTerm,
                            int leaderID,
                            int prevLogIndex,
                            int prevLogTerm,
                            Entry[] entries,
                            int leaderCommit) {
    //stale leader
    //check the leaders term. if its greater than me return to follower mode and change my term to theirs
    //if you have a higher term i think you return your own term
    //
    synchronized (mLock) {
      int selfTerm = mConfig.getCurrentTerm();
      if(leaderTerm > selfTerm) {
        mConfig.setCurrentTerm(leaderTerm, 0);
        //RaftResponses.setTerm(leaderTerm);
        bigTimer.cancel();
        smallTimer.cancel();
        RaftServerImpl.setMode(new FollowerMode());
        return 0; // is this right? 
      }
      else {
        return selfTerm;
      }
    }
  }

  // @param id of the timer that timed out
  public void handleTimeout (int timerID) {
    synchronized (mLock) {
      int selfTerm = mConfig.getCurrentTerm();
      int higherTerm = 0;
      //int server=mID;
      int max=0;

      if(timerID==2){
        bigTimer.cancel();
        smallTimer.cancel();
        RaftResponses.clearVotes(selfTerm);
        startElection();
      }
      
      if(timerID == 3) {
        int numVotes = 0;
        int[] votes = RaftResponses.getVotes(selfTerm);
        for(int i = 0; i < votes.length; i++) {
          if(votes[i] == 0) {
            numVotes++;
          }
          if(votes[i]>selfTerm){
            max=votes[i];
            //server=i;
          }
        }
        if(max>selfTerm){
            mConfig.setCurrentTerm(max, 0);
            bigTimer.cancel();
            smallTimer.cancel();
            RaftServerImpl.setMode(new FollowerMode());
            return;
        }
        // if this candidate has a majority
        // + 1 takes into account vote for self
        else if(numVotes + 1 > (mConfig.getNumServers())/2){
          //we were missing any cancels in this statement before at all...
          //this honestly might have been messing us up a LOT
          bigTimer.cancel();
          smallTimer.cancel();
          RaftServerImpl.setMode(new LeaderMode());
          return;
        }
        else{
          //keep waiting for more votes
          smallTimer.cancel();
          smallTimer = scheduleTimer(25, 3);
        }
      }    
    }
  }

  public boolean logCheck(int lastLogIndex, int lastLogTerm){
    int myLastTerm = mLog.getLastTerm();
    int myLastIndex = mLog.getLastIndex();
    if(lastLogTerm>myLastTerm){
      //its upto date
      return true;
    }
    else if(lastLogIndex>=myLastIndex && lastLogTerm==myLastTerm){
      return true;
    }
    else{
      return false; //not up to date 
    }

  }

}