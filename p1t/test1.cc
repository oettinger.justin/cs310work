#include <iostream>
#include "thread.h"

using namespace std;

//testing libinit and create work properly
void thread(void* arg)
{
    cout << "I am thread" << flush << endl;
}

void setup(void* arg)
{
    cout << "I am libinit" << flush << endl;
    thread_create(thread, NULL);
}

int main()
{
    cout << "I am main" << endl;
    thread_libinit(setup, NULL);
}