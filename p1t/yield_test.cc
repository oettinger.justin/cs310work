#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
#include "thread.h"

using namespace std;

size_t yieldNum = 2;

void thread_checker(void* input) {
  int threadNum = *((int*) input);
  cout << "Thread " << threadNum << " is running..." << endl;
  cout << "Current yieldNum: " << yieldNum << endl;
  if(threadNum != yieldNum) {
    cout << "threadNum " << threadNum << " does not equal " << yieldNum << endl;
    thread.yield();
  }
  cout << "We have a match! " << threadNum << " equals " << yieldNum << endl;
  yieldNum--;
}

//A function for libinit to run
void start(void* input) {
  //create 3 threads
  for (int i = 0; i < 3; i++) {
    int counter = i;
    int* counterlocation = &counter;
    thread_create((thread_startfunc_t) thread_checker, (void*) counterlocation);
  }
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  if(thread_libinit((thread_startfunc_t) start, (void*) argv)){
    cout << "thread_libinit failed."
    return -1;
  }
  return 0;
}
