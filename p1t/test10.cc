#include <stdlib.h>
#include <iostream>
#include "thread.h"

using namespace std;


void dummy_func(void *arg) {}

int main() {
  //1. test thread_create returns -1 if library is not initialized
  if (thread_create(dummy_func, NULL) != -1) {
    cout << "thread_create should fail since thread library not intialized\n";
    exit(1);
  }
  if (thread_broadcast(1, 1) != -1) {
    cout << "thread_broadcast should fail since thread library not intialized\n";
    exit(1);
  }
  if (thread_signal(1, 1) != -1) {
    cout << "thread_signal should fail since thread library not intialized\n";
    exit(1);
  }
  if (thread_lock(1) != -1) {
    cout << "thread_lock should fail since thread library not intialized\n";
    exit(1);
  }
  if (thread_unlock(1) != -1) {
    cout << "thread_unlock should fail since thread library not intialized\n";
    exit(1);
  }
  if (thread_yield() != -1) {
    cout << "thread_yield should fail since thread library not intialized\n";
    exit(1);
  }
  if(thread_wait(1, 1) != -1) {
    cout << "thread_wait should fail since thread library not intialized\n";
    exit(1);
  }
  return 0;
}