#include <stdlib.h>
#include <iostream>
#include "thread.h"

using namespace std;


void dummy_func() {}

bool i_have_run = false;

void should_be_run(void *arg) {
    i_have_run = true;
    if(thread_lock(1)) {
        cout << "couldn't lock properly on first lock" << endl;
        exit (1);
    }
    if(!thread_lock(1)) {
        cout << "didn't detect trying to relock a lock this thread holds" << endl;
        exit(1);
    }
}

void setup_function(void *arg) {
  //3. test thread_create returns 0 immediately on success
  if (thread_create(should_be_run, NULL) != 0) {
      cout << "thread_create returned nonzero\n";
      exit(1);
  }
}

//testing thread_create, libinit, 
int main() {
  //set up thread lib to test further, return -1 if breaks and exit
  if (thread_libinit( (thread_startfunc_t) parent, NULL) {
    cout << "thread_libinit failed\n";
    exit(1);
  }  
  
  return 0;
}