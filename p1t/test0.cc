#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <list>
#include <iterator>
#include "thread.h"

using namespace std;

//simple starter program only to check libinit and create
int counter = 0;

void printer(void* start) {
    cout << "inside printer" << endl;
    int startnum = *((int*) start);
    int plusone = startnum + 1;
    int plustwo = startnum + 2;
    cout << startnum << " " << plusone << " " << plustwo << endl;
}

void setup(void* args) {
    cout << "started setup func" << endl;
    for(int i = 0; i < 3; i++) {
        //int counter = 0;
        int* counterloc = &counter;
        cout << counter << endl;
        cout << counterloc << endl;
        cout << " starting thread " << i << endl;
        if(thread_create((thread_startfunc_t) printer, (void*) counterloc)) {
            cout << "thread_create failed\n";
            exit(1);
        }
        cout << " started thread " << i << endl;
        counter = counter + 3;
    }
    if (counter != 9) {
        cout << "threads running too early\n";
        exit(1);
    }
    cout << "returning from setup function" << endl;
    cout << endl;
}

int main(int argc, char* argv[]) {
    if (thread_libinit((thread_startfunc_t) setup, (void *) argv)) {
        cout << "thread_libinit failed\n";
        exit(1);
    }
    cout << "Returning from main" << endl;
    return 0;
}