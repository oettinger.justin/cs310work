#include <stdlib.h>
#include <iostream>
#include "thread.h"

using namespace std;


void should_be_run_2(void *arg) {
    if(thread_unlock(1) == 0) {
        cout << "library didn't prevent a different thread from unlocking a locked lock" << endl;
        exit(1);
    }
}

void should_be_run(void *arg) {
    if(thread_lock(1) == -1) {
        cout << "couldn't lock properly on first lock" << endl;
        exit (1);
    }
    thread_yield();
    cout << "back in should_be_run" << endl;
}

void setup_function(void *arg) {
  //3. test thread_create returns 0 immediately on success
  if (thread_create(should_be_run, NULL) != 0) {
      cout << "thread_create returned nonzero\n";
      exit(1);
  }
  if (thread_create(should_be_run_2, NULL) != 0) {
      cout << "thread_create returned nonzero\n";
      exit(1);
  }
}

//testing thread_create, libinit, 
int main() {
  //set up thread lib to test further, return -1 if breaks and exit
  if (thread_libinit( (thread_startfunc_t) setup_function, NULL)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }  
  
  return 0;
}