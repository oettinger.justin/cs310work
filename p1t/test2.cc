#include <iostream>
#include <stdint.h>
#include "thread.h"

using namespace std;

//testing to see whether locks work properly
void thread_1(void *arg)
{
    uintptr_t id = (uintptr_t)arg;
    //cout << "I am thread " << id << flush << endl;
    thread_lock(id);
    //cout << "thread " << id << " has lock 1" << flush << endl;
    thread_yield();
    //cout << "I am thread " << id << flush << endl;
    thread_unlock(id);
    //cout << "dying thread " << id << flush << endl;
}

void thread_2(void *arg)
{
    uintptr_t id = (uintptr_t)arg;
    //cout << "I am thread " << id << flush << endl;
    //cout << "Acquiring lock. thread " << id << flush << endl;
    thread_lock(id);
    //cout << "thread " << id << " has lock 1" << flush << endl;
    thread_unlock(id);
    //cout << "dying thread " << id << flush << endl;
}

void setup(void *arg)
{
    cout << "I am libinit" << flush << endl;
    for (int i = 0; i < 10; i++)
    {
        if (i % 2 == 0)
            thread_create(thread_1, (void *)i);
        else
            thread_create(thread_2, (void *)i);
    }
}

int main()
{
    cout << "I am main" << endl;
    thread_libinit(setup, NULL);
}