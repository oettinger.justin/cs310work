#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int sum = 0;

void adder(void *arg) {
  int *x = (int*) arg;
  sum += *x;
}

void setup_func(void *arg) {
  int other_sum = 0;
  for (int i = 0; i < 50; i++) {
    int * x = new int;
    *x = i;
    thread_create(adder, x);
    other_sum += i;
  }
  for (int i = 0; i < 50; i++) {
    thread_yield();
  }
  if (sum != other_sum) {
    cout << "yield/create failed\n";
    exit(1);
  }
}

int main() {
  if (thread_libinit(setup_func, (void *) NULL)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}