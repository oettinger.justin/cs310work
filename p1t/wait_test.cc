#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
#include "thread.h"

using namespace std;

size_t lockOne = 1000;
size_t waitCond = 2000;
size_t lockTwo = 3000;


void thread_checker(void* input) {
  int *threadNum = (int*) input;
  cout << "Thread " << threadNum << " is running..." << endl;
  //thread 0 will try to wait on a lock it doesn't own
  if(threadNum == 0) {
    //to begin, thread 0 will create and release the lock
    thread_lock(lockOne);
    thread_unlock(lockOne);
    cout << "Thread 0 is about to try to wait on lockone, which it just released." << endl;
    thread_wait(lockOne);
    cout << "Thread 0 has waited on lockOne. This should be impossile." << endl;
    return -1;
  }
  //thread 1 will lock and then wait
  if(threadNum == 1) {
    thread_lock(lockOne);
    cout << "Thread 1 has locked on lockOne" << endl;
    thread_wait(lockOne, waitCond);
    cout << "Thread 1 has returned!" << end;
  }
  //thread 2 will wait on a nonexistent lock
  if (threadNum == 2) {
    cout << "Thread 2 is about to try to wait on a nonexistent lock." << endl;
    thread_wait(666);
    cout << "Thread 2 has waited on 666. This should be impossible. The Apocalypse is nigh." << endl;
    return -1;
  }

  //dont forget to unlock on lockOne (thread 1)
  thread_signal(lockOne,waitCond);
  thread_unlock(lockOne);
  return 0;
}

//A function for libinit to run
void start(void* input) {
  //create 3 threads
  for (int i = 0; i < 3; i++) {
    thread_create((thread_startfunc_t) thread_checker, (void*) i);
  }
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  if(thread_libinit((thread_startfunc_t) start, (void*) argv)){
    cout << "thread_libinit failed."
    return -1;
  }
  return 0;
}
