#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
#include "thread.h"

using namespace std;

size_t lockOne = 1000;
size_t lockTwo = 2000;

void thread_checker(void* input) {
  int *threadNum = (int*) input;
  cout << "Thread " << threadNum << " is running..." << endl;

  //thread 0 will lock and then yield
  if(threadNum == 0) {
    thread_lock(lockOne);
    cout << "Thread 0 has locked on lockOne" << endl;
    thread_yield();
  )
  //thread 1 will try to grab on thread 0's lock
  if (threadNum == 1) {
    cout << "Thread 1 is trying to lock on lockOne." << endl;
    thread_lock(lockOne);
    cout << "Thread 1 has locked on lockOne. This should be impossible." << endl;
    return -1;
  }
  //thread 2 will try to unlock a lock it does not hold
  if (threadNum == 2) {
    cout << "Thread 2 is trying to release lockTwo." << endl;
    thread_unlock(lockTwo);
    cout << "Thread 2 has unlocked lockTwo. This should be impossible." << endl;
    return -1;
  }
  //thread 3 will try to unlock a lock that has not been initiated
  if (threadNum == 3) {
    cout << "Thread 3 is trying to release an imaginary lock." << endl;
    thread_unlock(666);
    cout << "Thread 3 has unlocked 666. This should be impossible. The Apocalypse is nigh." << endl;
    return -1;
  }
  //dont forget to unlock on lockOne (thread 0)
  thread_unlock(lockOne);
  return 0;
}

//A function for libinit to run
void start(void* input) {
  //create 4 threads
  for (int i = 0; i < 4; i++) {
    thread_create((thread_startfunc_t) thread_checker, (void*) i);
  }
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  if(thread_libinit((thread_startfunc_t) start, (void*) argv)){
    cout << "thread_libinit failed."
    return -1;
  }
  return 0;
}
