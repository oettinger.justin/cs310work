#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
#include "thread.h"

using namespace std;

//A function for libinit to run
void start(void* input) {
  if(LIB_INITIATED == 1) {
    cout << "Thread Library initiated..." << endl;
  }
  else {
    cout << "Thread Library not initiated." << endl;
    exit(0);
  }
  //create 3 threads
  size_t count = 3;
  for (size_t i = 0; i < count; i++) {
    thread_create((thread_startfunc_t) thread_checker, (void*) i);
  }
}

void thread_checker(void* input) {
  int *threadNum = (int*) input;
  cout << "Thread " << threadNum << " is running..." << endl;
  exit(0);
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  thread_libinit((thread_startfunc_t) start, (void*) argv[1]);
  return 0;
}
