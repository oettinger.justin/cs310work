#include <iostream>
#include <ucontext.h>
#include <queue>
#include <map>
#include <cstdlib>
#include "thread.h"
#include "interrupt.h"

#define DBUG() cout << "line" << __LINE__ << endl

using namespace std;
struct tcb_t
{
    size_t id;
    ucontext_t *thread;
};

size_t LIB_INITIATED = 0;
size_t THREAD_COUNTER = 1;
tcb_t *current_thread = NULL;

map<size_t, size_t> LOCK_HELD;
map<size_t, queue<tcb_t *> *> LOCK_WAIT;
map<pair<size_t, size_t>, queue<tcb_t *> *> CONDVARS;

queue<tcb_t *> READY_QUEUE;
queue<tcb_t *> ZOMBIE;


int clean_zombies()
{
    while (!ZOMBIE.empty())
    {
        tcb_t *toClean = ZOMBIE.front();
        ZOMBIE.pop();
        delete[] (char*) toClean->thread->uc_stack.ss_sp;
        delete toClean->thread;
        delete toClean;
    }
}

int thread_func_wrapper(thread_startfunc_t func, void *arg)
{
    clean_zombies();
    interrupt_enable();
    func(arg);
    interrupt_disable();
    clean_zombies();
    if (READY_QUEUE.empty())
    {
        cout << "Thread library exiting.\n";
        interrupt_enable();
        exit(0);
    }
    else
    {
        ZOMBIE.push(current_thread);
        tcb_t *to_run = READY_QUEUE.front();
        READY_QUEUE.pop();
        current_thread = to_run;        
        setcontext(to_run->thread);
        DBUG();
    }
    
    return 0;
}

int thread_libinit(thread_startfunc_t func, void *arg)
{
    if (LIB_INITIATED == 1)
    {
        //cout << "thread_libinit() already called." << endl;
        return -1;
    }
    LIB_INITIATED = 1;
    thread_create(func, arg);
    tcb_t *to_run = READY_QUEUE.front();
    READY_QUEUE.pop();
    current_thread = to_run;
    interrupt_disable();
    setcontext(to_run->thread);
}

int thread_create(thread_startfunc_t func, void *arg)
{
    interrupt_disable();
    clean_zombies();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    tcb_t *newThread;
    try
    {
        newThread = new tcb_t;
    }
    catch (const bad_alloc &e)
    {
        interrupt_enable();
        return -1;
    }
    newThread->id = THREAD_COUNTER++;
    try
    {
        newThread->thread = new ucontext_t;
    }
    catch (const bad_alloc &e)
    {
        delete newThread;
        interrupt_enable();
        return -1;
    }

    char *stack;
    try
    {
        stack = new char[STACK_SIZE];
    }
    catch (const bad_alloc &e)
    {
        delete newThread->thread;
        delete newThread;
        interrupt_enable();
        return -1;
    }
    newThread->thread->uc_stack.ss_sp = stack;
    newThread->thread->uc_stack.ss_size = STACK_SIZE;
    newThread->thread->uc_stack.ss_flags = 0;
    newThread->thread->uc_link = NULL;

    getcontext(newThread->thread);
    makecontext(newThread->thread, (void (*)())thread_func_wrapper, 2, func, arg);
    READY_QUEUE.push(newThread);
    interrupt_enable();
    return 0;
}

int run_off_ready_q()
{
    clean_zombies();
    tcb_t *old_thread = current_thread;
    current_thread = READY_QUEUE.front();
    READY_QUEUE.pop();
    swapcontext(old_thread->thread, current_thread->thread);
    current_thread = old_thread;
    clean_zombies();
    return 0;
}

int thread_yield(void)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    READY_QUEUE.push(current_thread);
    run_off_ready_q();
    interrupt_enable();
    return 0;
}

int thread_lock(unsigned int lock)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }

    if (LOCK_WAIT.find(lock) == LOCK_WAIT.end())
    {
        LOCK_HELD.insert(make_pair(lock, current_thread->id));
        queue<tcb_t *> *waiting;
        try
        {
            waiting = new queue<tcb_t *>;
        }
        catch (const bad_alloc &e)
        {
            interrupt_enable();
            return -1;
        }
        LOCK_WAIT.insert(make_pair(lock, waiting));
    }
    else if (LOCK_HELD[lock] == current_thread->id)
    {
        //cout << "Tried to lock a lock that thread already holds" << endl;
        interrupt_enable();
        return -1;
    }
    else if (LOCK_HELD[lock] == 0)
    {
        LOCK_HELD[lock] = current_thread->id;
    }
    else
    {
        LOCK_WAIT[lock]->push(current_thread);
        run_off_ready_q();
    }
    interrupt_enable();
    return 0;
}

int thread_unlock(unsigned int lock)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    if (LOCK_WAIT.find(lock) == LOCK_WAIT.end())
    {
        //cout << "Tried to unlock a lock that doesn't exist." << endl;
        interrupt_enable();
        return -1;
    }

    if (LOCK_HELD[lock] == 0)
    {
        //cout << "Tried to unlock a lock that isn't locked." << endl;
        interrupt_enable();
        return -1;
    }
    if (LOCK_HELD[lock] != current_thread->id)
    {
        //cout << "Thread " << current_thread->id << " tried to unlock a lock that " << LOCK_HELD[lock] << "holds" << flush << endl;
        interrupt_enable();
        return -1;
    }

    LOCK_HELD[lock] = 0;
    if (!LOCK_WAIT[lock]->empty())
    {
        tcb_t *nextHolder = LOCK_WAIT[lock]->front();
        LOCK_HELD[lock] = nextHolder->id;
        LOCK_WAIT[lock]->pop();
        READY_QUEUE.push(nextHolder);
    }
    interrupt_enable();
    return 0;
}
int thread_wait(unsigned int lock, unsigned int cond)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    if (LOCK_WAIT.find(lock) == LOCK_WAIT.end())
    {
        //cout << "Tried to unlock a lock that doesn't exist." << endl;
        interrupt_enable();
        return -1;
    }
    if (LOCK_HELD[lock] == 0)
    {
        //cout << "Tried to unlock a lock that isn't locked." << endl;
        interrupt_enable();
        return -1;
    }
    if (LOCK_HELD[lock] != current_thread->id)
    {
        //cout << "Tried to unlock a lock that this thread doesn't hold." << endl;
        interrupt_enable();
        return -1;
    }
    pair<size_t, size_t> thisPair = make_pair(lock, cond);
    if (CONDVARS.find(thisPair) == CONDVARS.end())
    {
        try
        {
            queue<tcb_t *> *cvQ = new queue<tcb_t *>;
            cvQ->push(current_thread);
            CONDVARS.insert(make_pair(thisPair, cvQ));
        }
        catch (const bad_alloc &e)
        {
            interrupt_enable();
            return -1;
        }   
    }
    else
    {
        CONDVARS[thisPair]->push(current_thread);
    }

    LOCK_HELD[lock] = 0;
    if (!LOCK_WAIT[lock]->empty())
    {
        tcb_t *nextHolder = LOCK_WAIT[lock]->front();
        LOCK_HELD[lock] = nextHolder->id;
        LOCK_WAIT[lock]->pop();
        READY_QUEUE.push(nextHolder);
    }

    run_off_ready_q();

    if (LOCK_WAIT.find(lock) == LOCK_WAIT.end())
    {
        LOCK_HELD.insert(make_pair(lock, current_thread->id));
        try
        {
            queue<tcb_t *> *waiting = new queue<tcb_t *>;
            LOCK_WAIT.insert(make_pair(lock, waiting));
        }
        catch (const bad_alloc &e)
        {
            interrupt_enable();
            return -1;
        }
    }
    else if (LOCK_HELD[lock] == 0)
    {
        LOCK_HELD[lock] = current_thread->id;
    }
    else
    {
        LOCK_WAIT[lock]->push(current_thread);
        run_off_ready_q();
    }
    interrupt_enable();
    return 0;
}

int thread_signal(unsigned int lock, unsigned int cond)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    pair<size_t, size_t> thisPair = make_pair(lock, cond);
    if (CONDVARS.find(thisPair) != CONDVARS.end())
    {
        if (!CONDVARS[thisPair]->empty())
        {
            tcb_t *toWakeup = CONDVARS[thisPair]->front();
            CONDVARS[thisPair]->pop();
            READY_QUEUE.push(toWakeup);
        }

        if (CONDVARS[thisPair]->empty())
        {
            delete CONDVARS[thisPair];
            CONDVARS.erase(thisPair);
        }
    }
    interrupt_enable();
    return 0;
}
int thread_broadcast(unsigned int lock, unsigned int cond)
{
    interrupt_disable();
    if (LIB_INITIATED == 0)
    {
        //cout << "thread_libinit() not called." << endl;
        interrupt_enable();
        return -1;
    }
    pair<size_t, size_t> thisPair = make_pair(lock, cond);
    if (CONDVARS.find(thisPair) != CONDVARS.end())
    {
        while (!CONDVARS[thisPair]->empty())
        {
            tcb_t *toWakeup = CONDVARS[thisPair]->front();
            CONDVARS[thisPair]->pop();
            READY_QUEUE.push(toWakeup);
        }
        delete CONDVARS[thisPair];
        CONDVARS.erase(thisPair);
    }
    interrupt_enable();
    return 0;
}
