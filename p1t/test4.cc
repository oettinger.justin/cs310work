#include <stdlib.h>
#include <iostream>
#include "thread.h"

using namespace std;


void dummy_func(void *arg) {}

bool i_have_run = false;
void should_be_run(void *arg) {
    i_have_run = true;
}

void setup_function(void *arg) {
  //2. test libinit should return -1 if called twice
  if(thread_libinit(dummy_func, NULL) != -1) {
      cout << "thread library doesn't detect double libinit calls\n";
      exit(1);
  }
  //3. test thread_create returns 0 immediately on success
  if (thread_create(should_be_run, NULL) != 0) {
      cout << "thread_create returned nonzero\n";
      exit(1);
  }
  //4. tests thread_yield returns 0 on success
  if(thread_yield() != 0) {
      cout << "thread yield improper return\n";
      exit(1);
  }
  //5. tests that a thread we make actually runs
  if(i_have_run == false) {
      cout << "thread that create made didn't actually run\n";
      exit(1);
  }

}

//testing thread_create, libinit, 
int main() {
  //1. test thread_create returns -1 if library is not initialized
  if (thread_create(dummy_func, NULL) != -1) {
    cout << "thread_create failed\n";
    exit(1);
  }
  
  //set up thread lib to test further, return -1 if breaks and exit
  if (thread_libinit( (thread_startfunc_t) setup_function, NULL)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }  
  
  return 0;
}