#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
#include "thread.h"
#include "thread.cc"
using namespace std;

size_t waitThreshold = 2;
size_t waitingThreads = 0;
size_t signaled = 1;
size_t waitLock = 1000;
size_t waitCond = 2000;

//A function for libinit to run
void start(void* input) {
  if(LIB_INITIATED == 1) {
    cout << "Thread Library initiated..." << endl;
  }
  else {
    cout << "Thread Library not initiated." << endl;
    exit(0);
  }
  //create 3 threads
  size_t count = 3;
  for (size_t i = 0; i < count; i++) {
    thread_create((thread_startfunc_t) thread_checker, (void*) i);
  }
}

void thread_checker(void* input) {
  int *threadNum = (int*) input;
  cout << "Thread " << threadNum << " is running..." << endl;
  cout << "Current # of waiting threads: " << waitingThreads << endl;
  thread_lock(waitLock);
  while(waitingThreads != waitThreshold) {
    cout << "Not ready to signal!"
    waitingThreads++;
    thread.wait(waitLock, waitCond);
  }
  cout << "Ready! " << waitingThreads << " are waiting and our wait threshold is " << waitThreshold << endl;
  thread_broadcast(waitLock, waitCond);
  cout << "We have broadcasted!"
  cout << "There are " <<  READY_QUEUE.size() << " elements in the ready queue!" << endl;
  thread_unlock(waitLock);
  exit(0);
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  thread_libinit((thread_startfunc_t) start, (void*) argv[1]);
  return 0;
}
