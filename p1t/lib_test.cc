#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <iterator>
//#include "thread.h"
#include "thread.cc"
using namespace std;

//A function for libinit to run
// void func(void* input) {
//   int *num = (int*) input;
//   if(LIB_INITIATED == 1) {
//     cout << "Thread Library initiated..." << endl;
//     cout << "You entered the number: " << num << endl;
//     exit(0);
//   }
//     cout << "Thread Library not initiated." << endl;
//     exit(0);
// }

void otherFunc(void* input) {
  exit(0);
}

void func(void* input) {
  thread_libinit((thread_startfunc_t) otherFunc, input);
  exit(0);
}

//call Library Initiation in Main
int main(int argc, char* argv[]) {
  thread_libinit((thread_startfunc_t) func, (void*) argv[1]);
  return 0;
}
